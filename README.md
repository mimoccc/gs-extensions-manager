# gs-extensions-manager

Custom gui application to manage extensions in another way.
- backup/restore extensions settings
- check extension status after gnome shell starts
- update or downgrade extensions
- notify user about extension fault
- recover extension state
& etc